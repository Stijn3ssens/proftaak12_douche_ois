﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartDoucheSchets
{
    public partial class DoucheForm : Form
    {

        int[,] values = new int[3, 3];
       
        public DoucheForm()
        {
            InitializeComponent();
            for(int i = 0; i < 3; i++)
            {
                values[i, 0] = 100;
                values[i, 1] = 100;
                values[i, 2] = 100;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(trackBar1.Value.ToString() + " " + trackBar2.Value.ToString() + " " + trackBar3.Value.ToString() + " " + numericUpDown2.Value.ToString());
            Douchen douchen = new Douchen(trackBar1.Value, trackBar2.Value, trackBar3.Value);
            douchen.Show();
            this.Hide();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        int tag;
        private void checkbox_clock(object sender, EventArgs e)
        {
            CheckBox C = (CheckBox)sender;
            tag = Convert.ToInt16(C.Tag);

            trackBar1.Value = values[tag, 0];
            trackBar2.Value = values[tag, 1];
            trackBar3.Value = values[tag, 2];

            string[] lichaamsdeel = { "Hoofd", "Rug", "Voeten" };
            LbSelected.Text = lichaamsdeel[tag];
        }
        int index;
        private void valueChoinged(object sender, EventArgs e)
        {
            TrackBar T = (TrackBar)sender;
            index = Convert.ToInt16(T.Tag);
            values[tag, index] = T.Value;
        }
    }
}