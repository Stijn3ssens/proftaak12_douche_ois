﻿SELECT account.naam, AVG(Tijd) AS "Gemiddelde DoucheTijd"
FROM douchebeurt
INNER JOIN account ON douchebeurt.AccountId = account.Id
GROUP BY account.naam, AccountId
ORDER BY AVG(Tijd) ASC




/*
	Deze Query rekent uit wat de gemiddelde duur is van een behandeling per persoon
*/