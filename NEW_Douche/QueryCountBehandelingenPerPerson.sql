﻿SELECT account.naam AS Gebruiker, COUNT(AccountId) AS "Aantal Behandelingen"
FROM douchebeurt
INNER JOIN account ON douchebeurt.AccountId = account.Id
GROUP BY account.naam, AccountId
ORDER BY COUNT(AccountId) DESC

/*
	Er wordt geteld hoeveel behandelingen iedere persoon heeft
*/