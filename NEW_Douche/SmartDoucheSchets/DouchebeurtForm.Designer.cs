﻿namespace SmartDoucheSchets
{
    partial class DouchebeurtForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
            this.LvBehandelingen = new System.Windows.Forms.ListView();
            this.BehandelingsNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ATZeep = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Tijd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.BtStart = new System.Windows.Forms.Button();
            this.doucheTimer = new System.Windows.Forms.Timer(this.components);
            this.BtBack = new System.Windows.Forms.Button();
            this.BtVerwijder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LvBehandelingen
            // 
            this.LvBehandelingen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.BehandelingsNaam,
            this.ATZeep,
            this.Tijd});
            this.LvBehandelingen.FullRowSelect = true;
            this.LvBehandelingen.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.LvBehandelingen.Location = new System.Drawing.Point(22, 26);
            this.LvBehandelingen.Margin = new System.Windows.Forms.Padding(2);
            this.LvBehandelingen.Name = "LvBehandelingen";
            this.LvBehandelingen.Size = new System.Drawing.Size(564, 218);
            this.LvBehandelingen.TabIndex = 0;
            this.LvBehandelingen.UseCompatibleStateImageBehavior = false;
            this.LvBehandelingen.View = System.Windows.Forms.View.Details;
            this.LvBehandelingen.SelectedIndexChanged += new System.EventHandler(this.LvBehandelingen_SelectedIndexChanged);
            // 
            // BehandelingsNaam
            // 
            this.BehandelingsNaam.Text = "Behandelingsnaam";
            this.BehandelingsNaam.Width = 216;
            // 
            // ATZeep
            // 
            this.ATZeep.Text = "Aantal Zeep";
            this.ATZeep.Width = 173;
            // 
            // Tijd
            // 
            this.Tijd.Text = "Tijd in seconden";
            this.Tijd.Width = 169;
            // 
            // BtStart
            // 
            this.BtStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtStart.Location = new System.Drawing.Point(457, 269);
            this.BtStart.Margin = new System.Windows.Forms.Padding(2);
            this.BtStart.Name = "BtStart";
            this.BtStart.Size = new System.Drawing.Size(128, 87);
            this.BtStart.TabIndex = 1;
            this.BtStart.Text = "Start!";
            this.BtStart.UseVisualStyleBackColor = true;
            this.BtStart.Click += new System.EventHandler(this.BtStart_Click);
            // 
            // BtBack
            // 
            this.BtBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtBack.Location = new System.Drawing.Point(22, 269);
            this.BtBack.Margin = new System.Windows.Forms.Padding(2);
            this.BtBack.Name = "BtBack";
            this.BtBack.Size = new System.Drawing.Size(128, 87);
            this.BtBack.TabIndex = 2;
            this.BtBack.Text = "Terug";
            this.BtBack.UseVisualStyleBackColor = true;
            this.BtBack.Click += new System.EventHandler(this.BtBack_Click);
            // 
            // BtVerwijder
            // 
            this.BtVerwijder.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtVerwijder.Location = new System.Drawing.Point(244, 269);
            this.BtVerwijder.Margin = new System.Windows.Forms.Padding(2);
            this.BtVerwijder.Name = "BtVerwijder";
            this.BtVerwijder.Size = new System.Drawing.Size(128, 87);
            this.BtVerwijder.TabIndex = 3;
            this.BtVerwijder.Text = "Verwijder";
            this.BtVerwijder.UseVisualStyleBackColor = true;
            this.BtVerwijder.Click += new System.EventHandler(this.BtVerwijder_Click);
            // 
            // DouchebeurtForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.BtVerwijder);
            this.Controls.Add(this.BtBack);
            this.Controls.Add(this.BtStart);
            this.Controls.Add(this.LvBehandelingen);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DouchebeurtForm";
            this.Text = "Douchen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DouchebeurtForm_FormClosed);
            this.Load += new System.EventHandler(this.DouchenbeurtForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView LvBehandelingen;
        private System.Windows.Forms.Button BtStart;
        private System.Windows.Forms.Timer doucheTimer;
        private System.Windows.Forms.ColumnHeader ATZeep;
        private System.Windows.Forms.ColumnHeader Tijd;
        private System.Windows.Forms.Button BtBack;
        private System.Windows.Forms.ColumnHeader BehandelingsNaam;
        private System.Windows.Forms.Button BtVerwijder;
    }
}