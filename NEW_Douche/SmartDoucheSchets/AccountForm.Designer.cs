﻿namespace SmartDoucheSchets
{
    partial class AccountForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtStartDouchebeurt = new System.Windows.Forms.Button();
            this.BtNieuwDouchebeurt = new System.Windows.Forms.Button();
            this.CbSelecteerAccount = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtMaakAccount = new System.Windows.Forms.Button();
            this.BtVerwijder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtStartDouchebeurt
            // 
            this.BtStartDouchebeurt.Location = new System.Drawing.Point(446, 198);
            this.BtStartDouchebeurt.Margin = new System.Windows.Forms.Padding(2);
            this.BtStartDouchebeurt.Name = "BtStartDouchebeurt";
            this.BtStartDouchebeurt.Size = new System.Drawing.Size(133, 121);
            this.BtStartDouchebeurt.TabIndex = 0;
            this.BtStartDouchebeurt.Text = "Douchen\r\n";
            this.BtStartDouchebeurt.UseVisualStyleBackColor = true;
            this.BtStartDouchebeurt.Click += new System.EventHandler(this.BtStartDouchebeurt_Click);
            // 
            // BtNieuwDouchebeurt
            // 
            this.BtNieuwDouchebeurt.Location = new System.Drawing.Point(446, 34);
            this.BtNieuwDouchebeurt.Margin = new System.Windows.Forms.Padding(2);
            this.BtNieuwDouchebeurt.Name = "BtNieuwDouchebeurt";
            this.BtNieuwDouchebeurt.Size = new System.Drawing.Size(133, 121);
            this.BtNieuwDouchebeurt.TabIndex = 1;
            this.BtNieuwDouchebeurt.Text = "Nieuwe DoucheBeurt";
            this.BtNieuwDouchebeurt.UseVisualStyleBackColor = true;
            this.BtNieuwDouchebeurt.Click += new System.EventHandler(this.BtNieuwDouchebeurt_Click);
            // 
            // CbSelecteerAccount
            // 
            this.CbSelecteerAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbSelecteerAccount.FormattingEnabled = true;
            this.CbSelecteerAccount.Location = new System.Drawing.Point(309, 136);
            this.CbSelecteerAccount.Margin = new System.Windows.Forms.Padding(2);
            this.CbSelecteerAccount.Name = "CbSelecteerAccount";
            this.CbSelecteerAccount.Size = new System.Drawing.Size(92, 21);
            this.CbSelecteerAccount.TabIndex = 2;
            this.CbSelecteerAccount.SelectedIndexChanged += new System.EventHandler(this.CbSelecteerAccount_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(289, 105);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Wie gaat er douchen?";
            // 
            // BtMaakAccount
            // 
            this.BtMaakAccount.Location = new System.Drawing.Point(35, 34);
            this.BtMaakAccount.Margin = new System.Windows.Forms.Padding(2);
            this.BtMaakAccount.Name = "BtMaakAccount";
            this.BtMaakAccount.Size = new System.Drawing.Size(133, 121);
            this.BtMaakAccount.TabIndex = 4;
            this.BtMaakAccount.Text = "Maak account";
            this.BtMaakAccount.UseVisualStyleBackColor = true;
            this.BtMaakAccount.Click += new System.EventHandler(this.BtMaakAccount_Click);
            // 
            // BtVerwijder
            // 
            this.BtVerwijder.Location = new System.Drawing.Point(35, 198);
            this.BtVerwijder.Margin = new System.Windows.Forms.Padding(2);
            this.BtVerwijder.Name = "BtVerwijder";
            this.BtVerwijder.Size = new System.Drawing.Size(133, 121);
            this.BtVerwijder.TabIndex = 5;
            this.BtVerwijder.Text = "Verwijder Account";
            this.BtVerwijder.UseVisualStyleBackColor = true;
            this.BtVerwijder.Click += new System.EventHandler(this.BtVerwijder_Click);
            // 
            // AccountForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.BtVerwijder);
            this.Controls.Add(this.BtMaakAccount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CbSelecteerAccount);
            this.Controls.Add(this.BtNieuwDouchebeurt);
            this.Controls.Add(this.BtStartDouchebeurt);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AccountForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Account selectie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AccountForm_FormClosing);
            this.Load += new System.EventHandler(this.AccountForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtStartDouchebeurt;
        private System.Windows.Forms.Button BtNieuwDouchebeurt;
        private System.Windows.Forms.ComboBox CbSelecteerAccount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtMaakAccount;
        private System.Windows.Forms.Button BtVerwijder;
    }
}

