﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartDoucheSchets
{
    public partial class AccountForm : Form
    {

        int persoonId = 0;
        List<int> PersoonsList = new List<int>();
        SQL_Queries queries = new SQL_Queries();

        public AccountForm()
        {
            InitializeComponent();
            this.Cursor = new Cursor(GetType(), "Cursor1.cur");
        }

        private void AccountGegevensOphalen()
        {
            CbSelecteerAccount.Items.Clear();

            List<string[]> AccountList = queries.GetAccount();
            foreach (string[] a in AccountList)
            {
                PersoonsList.Add(Convert.ToInt16(a[0]));
                CbSelecteerAccount.Items.Add(a[1]);
            }
        }

        private void BtStartDouchebeurt_Click(object sender, EventArgs e)
        {
            if (persoonId != 0)
            {
                DouchebeurtForm Douchen = new DouchebeurtForm(persoonId);
                this.Hide();
                Douchen.Show(); // nieuwe form douchen wordt aangemaakt en het ID van de gebruiker wordt doorgestuurd
            }
            else
            {
                MessageBox.Show("Selecteer eerst een account");
            }
        }

        private void BtNieuwDouchebeurt_Click(object sender, EventArgs e)
        {
            if (persoonId != 0)
            {
                AanmaakForm douche = new AanmaakForm(persoonId);
                douche.Show();
                this.Hide(); // nieuwe DoucheForm wordt aangemaakt en het ID van de gebruiker wordt doorgestuurd
            }
            else
            {
                MessageBox.Show("Selecteer eerst een account");
            }
        }

        private void BtMaakAccount_Click(object sender, EventArgs e)
        {
            AccountCreatieForm acc = new AccountCreatieForm();
            acc.Show();
            this.Hide(); // Het account registratie form wordt aangemaakt en geopend
        }

        private void AccountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit(); // Mocht het form op een niet gebruikelijke manier afgesloten worden wordt ook meteen de volledige applicatie gesloten
        }

        private void CbSelecteerAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            persoonId = PersoonsList[CbSelecteerAccount.SelectedIndex];
        }

        private void BtVerwijder_Click(object sender, EventArgs e)
        {
            if (queries.VerwijderAccount(persoonId))
            {
                MessageBox.Show("Het account is verwijderd");

                AccountGegevensOphalen();
                persoonId = 0;
            }
            else
            {
                MessageBox.Show("Verwijderen van het account en diens behandelingen is mislukt");
            }
        }

        private void AccountForm_Load(object sender, EventArgs e)
        {
            AccountGegevensOphalen();
        }
    }
}
