﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartDoucheSchets
{
    public partial class AccountCreatieForm : Form
    {
        public AccountCreatieForm()
        {
            InitializeComponent();
            CbGeslacht.DataSource = Enum.GetValues(typeof(Geslacht));
        }

        private void BtMaakAan_Click(object sender, EventArgs e)
        {
            if (TbNaam.Text != "" && TbLengte.Text != "")
            {
                try
                {
                    DoucheDataTableAdapters.DBAccountTableAdapter account = new DoucheDataTableAdapters.DBAccountTableAdapter();
                    SQL_Queries queries = new SQL_Queries();
                    account.QueryInsertAccount(TbNaam.Text, Convert.ToInt16(TbLengte.Text), Convert.ToInt16(CbGeslacht.SelectedValue));

                    AccountForm form = new AccountForm();
                    this.Close();
                    form.Show();
                }
                catch
                {
                    MessageBox.Show("Gelieve alle velden correct in te vullen", "Error");
                }
            }
        }

        private void TbLengte_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void BtBack_Click(object sender, EventArgs e)
        {
            AccountForm form = new AccountForm();
            form.Show();
            this.Hide();
        }
    }
}
