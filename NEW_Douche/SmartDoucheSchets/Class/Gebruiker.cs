﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets.Class
{
    public enum Geslacht
    {
        Man,
        Vrouw,
        Overig
    }

    class Gebruiker
    {
        public string Naam { get; set; }
        public string Lengte { get; set; }
        public Geslacht Geslacht { get; set; }

        public Gebruiker(string naam, string lengte, Geslacht geslacht)
        {
            Naam = naam;
            Lengte = lengte;
            Geslacht = geslacht;
        }
    }

}
