﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets.Class
{
    class MaakArduinoString
    {
        public int BaudRate { get; set; }
        public string PortName { get; set; }

        public MaakArduinoString(int baudRate, string portName)
        {
            BaudRate = baudRate;
            PortName = portName;
        }

        public string Stuur(string commando)
        {
            string stuurString = "#" + commando + "%";
            return stuurString;
        }

        public string StopMotoren()
        {
            string commando = "A0&B0";
            return Stuur(commando);
        }
    }
}
