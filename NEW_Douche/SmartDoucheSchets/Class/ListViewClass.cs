﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets.Class
{
    class ListViewClass
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public string AantalZeep { get; set; }
        public string Time { get; set; }

        public ListViewClass(int Id, string Naam, string AantalZeep, string Time)
        {
            this.Id = Id;
            this.Naam = Naam;
            this.AantalZeep = AantalZeep;
            this.Time = Time;
        }
    }
}
