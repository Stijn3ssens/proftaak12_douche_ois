﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets.Class
{
    class DoucheClassNew
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BorstelClassNew> Borstels { get; set; }
        public int Time { get; set; }
        public string MotorCommando { get; set; }

        public DoucheClassNew(int id, string name, List<BorstelClassNew> borstels, int time)
        {
            Id = id;
            Name = name;
            Borstels = borstels;
            Time = time;
        }

        public void MaakMotorCommando(string Naam, int BorstelSterkte)
        {
            MotorCommando += $"{Naam}1_{BorstelSterkte}&";
        }
    }
}
