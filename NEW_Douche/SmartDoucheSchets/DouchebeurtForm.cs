﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.IO.Ports;

namespace SmartDoucheSchets
{
    public partial class DouchebeurtForm : Form
    {
        int persoonId;
        private SerialPort LCDport;
        private SerialPort Motorport;

        Class.MaakArduinoString LCD = new Class.MaakArduinoString(9600, "COM3");
        Class.MaakArduinoString MOTOR = new Class.MaakArduinoString(9600, "COM7");

        Class.SQL_Queries queries = new Class.SQL_Queries();

        Timer DoucheTimer = new Timer();
        Class.DoucheClassNew Behandeling;
        List<int> IdList = new List<int>();

        private int tijdverstreken;

        public DouchebeurtForm(int persoonId)
        {
            InitializeComponent();
            this.persoonId = persoonId;
            persoonId = Convert.ToInt16(persoonId);
            this.Cursor = new Cursor(GetType(), "Cursor1.cur");
        }
        private void DouchebeurtForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                DoucheTimer.Stop();
                Motorport.WriteLine(MOTOR.StopMotoren());

                Motorport.Close();
                LCDport.Close();
            }
            catch { }
            finally
            {
                Application.Exit();
            }
        }

        public void ListView()
        {
            LvBehandelingen.Items.Clear();
            LvBehandelingen.View = View.Details;

            List<Class.ListViewClass> ListViewList = queries.MeerdereBorstelsOphalen(persoonId);

            foreach (Class.ListViewClass vlc in ListViewList)
            {
                ListViewItem item = new ListViewItem(vlc.Naam);
                item.SubItems.Add(vlc.AantalZeep);
                item.SubItems.Add(vlc.Time);
                LvBehandelingen.Items.Add(item);

                IdList.Add(vlc.Id);
            }
        }

        private void BtBack_Click(object sender, EventArgs e)
        {
            try
            {
                DoucheTimer.Stop();
                Motorport.WriteLine(MOTOR.StopMotoren());

                Motorport.Close();
                LCDport.Close();
            }
            catch
            {
                MessageBox.Show("ERROR");
            }
            finally
            {
                AccountForm form = new AccountForm();
                form.Show();
                this.Hide();
            }
        }

        private void Init()
        {
            try
            {
                LCDport = new SerialPort
                {
                    BaudRate = LCD.BaudRate,
                    PortName = LCD.PortName
                };
                LCDport.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Error with the connection of the LCD", "ERROR");
                LCDport.Close();
                BtStart.Enabled = false;
            }

            try
            {
                Motorport = new SerialPort
                {
                    BaudRate = MOTOR.BaudRate,
                    PortName = MOTOR.PortName
                };
                Motorport.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Error with the connection of the Motors", "ERROR");
                Motorport.Close();
                BtStart.Enabled = false;
                //Application.Restart();
            }
        }

        private void DouchenbeurtForm_Load(object sender, EventArgs e)
        {
            //Connecties met Arduino maken
            Init();
            ListView();

            DoucheTimer.Interval = 1000;
            DoucheTimer.Tick += new EventHandler(DoucheTimer_Tick);
        }

        private void BtStart_Click(object sender, EventArgs e)
        {
            int behandelingId = IdList[LvBehandelingen.FocusedItem.Index];
            string Name = queries.NaamOphalen(persoonId);

            List<Class.BorstelClassNew> borstels = queries.EenBorstelOphalen(behandelingId);

            int Time = queries.DoucheTimer(behandelingId);
            Console.WriteLine(Time.ToString());//debug
            Behandeling = new Class.DoucheClassNew(behandelingId, Name, borstels, Time);

            foreach (Class.BorstelClassNew bc in Behandeling.Borstels)
            {
                Behandeling.MaakMotorCommando(bc.Name, bc.Borstelsterkte);
            }

            //DEBUG
            Console.WriteLine(LCD.Stuur(Name));
            Console.WriteLine(MOTOR.Stuur(Behandeling.MotorCommando));

            //Berichten Sturen
            LCDport.WriteLine(LCD.Stuur(Name));
            Motorport.WriteLine(MOTOR.Stuur(Behandeling.MotorCommando));

            tijdverstreken = 0;
            DoucheTimer.Start();
        }

        private void DoucheTimer_Tick(object sender, EventArgs e)
        {
            tijdverstreken++;

            if (tijdverstreken > Behandeling.Time)
            {
                //De motoren worden allen gestopt
                Motorport.WriteLine(MOTOR.StopMotoren());

                DoucheTimer.Stop();
                MessageBox.Show("Klaar!");
            }
        }

        private void BtVerwijder_Click(object sender, EventArgs e)
        {
            try
            {
                if (queries.VerwijderBeurt(IdList[LvBehandelingen.FocusedItem.Index]))
                {
                    IdList.Remove(LvBehandelingen.FocusedItem.Index);
                    ListView();
                }
                else
                {
                    MessageBox.Show("Mislukt om de behandeling te verwijderen");
                }
            }
            catch
            {
                MessageBox.Show("Selecteer een item uit de display");
            }
        }

        private void LvBehandelingen_SelectedIndexChanged(object sender, EventArgs e)
        {
            int behandelingId = IdList[LvBehandelingen.FocusedItem.Index];
            string Name = queries.NaamOphalen(persoonId);

            List<Class.BorstelClassNew> borstels = queries.EenBorstelOphalen(behandelingId);

            int Time = queries.DoucheTimer(behandelingId);
            Console.WriteLine(Time.ToString() + " is de tijd");//debug
            Behandeling = new Class.DoucheClassNew(behandelingId, Name, borstels, Time);

            foreach (Class.BorstelClassNew bc in Behandeling.Borstels)
            {
                Behandeling.MaakMotorCommando(bc.Name, bc.Borstelsterkte);
            }

            //DEBUG
            Console.WriteLine(LCD.Stuur(Name) + " is de naam die wordt doorgestuurd");
            Console.WriteLine(MOTOR.Stuur(Behandeling.MotorCommando) + " is het commando dat wordt doorgestuurd");
        }
    }
}