﻿namespace SmartDoucheSchets
{
    partial class AccountCreatieForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbNaam = new System.Windows.Forms.TextBox();
            this.TbLengte = new System.Windows.Forms.TextBox();
            this.CbGeslacht = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtMaakAan = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtBack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TbNaam
            // 
            this.TbNaam.Location = new System.Drawing.Point(170, 48);
            this.TbNaam.Margin = new System.Windows.Forms.Padding(2);
            this.TbNaam.Name = "TbNaam";
            this.TbNaam.Size = new System.Drawing.Size(90, 24);
            this.TbNaam.TabIndex = 0;
            // 
            // TbLengte
            // 
            this.TbLengte.Location = new System.Drawing.Point(170, 77);
            this.TbLengte.Margin = new System.Windows.Forms.Padding(2);
            this.TbLengte.Name = "TbLengte";
            this.TbLengte.Size = new System.Drawing.Size(90, 24);
            this.TbLengte.TabIndex = 1;
            this.TbLengte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbLengte_KeyPress);
            // 
            // CbGeslacht
            // 
            this.CbGeslacht.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbGeslacht.FormattingEnabled = true;
            this.CbGeslacht.Location = new System.Drawing.Point(169, 108);
            this.CbGeslacht.Margin = new System.Windows.Forms.Padding(2);
            this.CbGeslacht.Name = "CbGeslacht";
            this.CbGeslacht.Size = new System.Drawing.Size(92, 26);
            this.CbGeslacht.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(82, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Naam:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(82, 93);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Lengte in cm:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(72, 113);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Geslacht:";
            // 
            // BtMaakAan
            // 
            this.BtMaakAan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtMaakAan.Location = new System.Drawing.Point(169, 153);
            this.BtMaakAan.Margin = new System.Windows.Forms.Padding(2);
            this.BtMaakAan.Name = "BtMaakAan";
            this.BtMaakAan.Size = new System.Drawing.Size(91, 28);
            this.BtMaakAan.TabIndex = 6;
            this.BtMaakAan.Text = "Maak aan";
            this.BtMaakAan.UseVisualStyleBackColor = true;
            this.BtMaakAan.Click += new System.EventHandler(this.BtMaakAan_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtMaakAan);
            this.groupBox1.Controls.Add(this.BtBack);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TbLengte);
            this.groupBox1.Controls.Add(this.CbGeslacht);
            this.groupBox1.Controls.Add(this.TbNaam);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(10, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(581, 345);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registreer";
            // 
            // BtBack
            // 
            this.BtBack.Location = new System.Drawing.Point(4, 274);
            this.BtBack.Margin = new System.Windows.Forms.Padding(2);
            this.BtBack.Name = "BtBack";
            this.BtBack.Size = new System.Drawing.Size(135, 67);
            this.BtBack.TabIndex = 8;
            this.BtBack.Text = "Terug";
            this.BtBack.UseVisualStyleBackColor = true;
            this.BtBack.Click += new System.EventHandler(this.BtBack_Click);
            // 
            // AccountCreatieForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AccountCreatieForm";
            this.Text = "Account aanmaken";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TbNaam;
        private System.Windows.Forms.TextBox TbLengte;
        private System.Windows.Forms.ComboBox CbGeslacht;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtMaakAan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtBack;
    }
}