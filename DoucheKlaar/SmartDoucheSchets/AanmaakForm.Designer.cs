﻿namespace SmartDoucheSchets
{
    partial class AanmaakForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TbLinks = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.BtAanmaken = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ChLinks = new System.Windows.Forms.CheckBox();
            this.ChRechts = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NudZeepPercentage = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.NudTijd = new System.Windows.Forms.NumericUpDown();
            this.LbSelected = new System.Windows.Forms.Label();
            this.TbRechts = new System.Windows.Forms.TrackBar();
            this.BtBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TbLinks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudZeepPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudTijd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbRechts)).BeginInit();
            this.SuspendLayout();
            // 
            // TbLinks
            // 
            this.TbLinks.Location = new System.Drawing.Point(261, 24);
            this.TbLinks.Margin = new System.Windows.Forms.Padding(2);
            this.TbLinks.Maximum = 100;
            this.TbLinks.Minimum = 1;
            this.TbLinks.Name = "TbLinks";
            this.TbLinks.Size = new System.Drawing.Size(399, 45);
            this.TbLinks.TabIndex = 0;
            this.TbLinks.Tag = "0";
            this.TbLinks.Value = 1;
            this.TbLinks.ValueChanged += new System.EventHandler(this.TrackbarValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sterkte Rechter Borstel:";
            // 
            // BtAanmaken
            // 
            this.BtAanmaken.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtAanmaken.Location = new System.Drawing.Point(500, 341);
            this.BtAanmaken.Margin = new System.Windows.Forms.Padding(2);
            this.BtAanmaken.Name = "BtAanmaken";
            this.BtAanmaken.Size = new System.Drawing.Size(167, 89);
            this.BtAanmaken.TabIndex = 3;
            this.BtAanmaken.Text = "Maak Aan!";
            this.BtAanmaken.UseVisualStyleBackColor = true;
            this.BtAanmaken.Click += new System.EventHandler(this.BtAanmaken_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sterkte Linker Borstel:";
            // 
            // ChLinks
            // 
            this.ChLinks.AutoSize = true;
            this.ChLinks.Location = new System.Drawing.Point(58, 141);
            this.ChLinks.Margin = new System.Windows.Forms.Padding(2);
            this.ChLinks.Name = "ChLinks";
            this.ChLinks.Size = new System.Drawing.Size(51, 17);
            this.ChLinks.TabIndex = 8;
            this.ChLinks.Tag = "0";
            this.ChLinks.Text = "Links";
            this.ChLinks.UseVisualStyleBackColor = true;
            // 
            // ChRechts
            // 
            this.ChRechts.AutoSize = true;
            this.ChRechts.Location = new System.Drawing.Point(58, 162);
            this.ChRechts.Margin = new System.Windows.Forms.Padding(2);
            this.ChRechts.Name = "ChRechts";
            this.ChRechts.Size = new System.Drawing.Size(60, 17);
            this.ChRechts.TabIndex = 9;
            this.ChRechts.Tag = "1";
            this.ChRechts.Text = "Rechts";
            this.ChRechts.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(46, 118);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "DoucheBeurten:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 243);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 24);
            this.label6.TabIndex = 14;
            this.label6.Text = "Aantal Zeep:";
            // 
            // NudZeepPercentage
            // 
            this.NudZeepPercentage.BackColor = System.Drawing.SystemColors.Control;
            this.NudZeepPercentage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NudZeepPercentage.DecimalPlaces = 1;
            this.NudZeepPercentage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NudZeepPercentage.Location = new System.Drawing.Point(160, 250);
            this.NudZeepPercentage.Margin = new System.Windows.Forms.Padding(2);
            this.NudZeepPercentage.Name = "NudZeepPercentage";
            this.NudZeepPercentage.Size = new System.Drawing.Size(68, 16);
            this.NudZeepPercentage.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(232, 244);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 24);
            this.label7.TabIndex = 16;
            this.label7.Text = "%";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(43, 193);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 24);
            this.label8.TabIndex = 17;
            this.label8.Text = "Tijd:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(188, 194);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 24);
            this.label9.TabIndex = 19;
            this.label9.Text = "Seconden";
            // 
            // NudTijd
            // 
            this.NudTijd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NudTijd.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NudTijd.Location = new System.Drawing.Point(93, 193);
            this.NudTijd.Margin = new System.Windows.Forms.Padding(2);
            this.NudTijd.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.NudTijd.Minimum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.NudTijd.Name = "NudTijd";
            this.NudTijd.Size = new System.Drawing.Size(90, 26);
            this.NudTijd.TabIndex = 20;
            this.NudTijd.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // LbSelected
            // 
            this.LbSelected.AutoSize = true;
            this.LbSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbSelected.Location = new System.Drawing.Point(397, 5);
            this.LbSelected.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LbSelected.Name = "LbSelected";
            this.LbSelected.Size = new System.Drawing.Size(0, 17);
            this.LbSelected.TabIndex = 21;
            // 
            // TbRechts
            // 
            this.TbRechts.Location = new System.Drawing.Point(261, 74);
            this.TbRechts.Margin = new System.Windows.Forms.Padding(2);
            this.TbRechts.Maximum = 100;
            this.TbRechts.Minimum = 1;
            this.TbRechts.Name = "TbRechts";
            this.TbRechts.Size = new System.Drawing.Size(399, 45);
            this.TbRechts.TabIndex = 4;
            this.TbRechts.Tag = "1";
            this.TbRechts.Value = 1;
            this.TbRechts.ValueChanged += new System.EventHandler(this.TrackbarValueChanged);
            // 
            // BtBack
            // 
            this.BtBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtBack.Location = new System.Drawing.Point(34, 341);
            this.BtBack.Margin = new System.Windows.Forms.Padding(2);
            this.BtBack.Name = "BtBack";
            this.BtBack.Size = new System.Drawing.Size(167, 89);
            this.BtBack.TabIndex = 22;
            this.BtBack.Text = "Terug";
            this.BtBack.UseVisualStyleBackColor = true;
            this.BtBack.Click += new System.EventHandler(this.BtBack_Click);
            // 
            // AanmaakForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(676, 440);
            this.Controls.Add(this.BtBack);
            this.Controls.Add(this.LbSelected);
            this.Controls.Add(this.NudTijd);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NudZeepPercentage);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ChRechts);
            this.Controls.Add(this.ChLinks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TbRechts);
            this.Controls.Add(this.BtAanmaken);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbLinks);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AanmaakForm";
            this.Text = "Behandeling aanmaken";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AanmaakForm_FormClosed);
            this.Load += new System.EventHandler(this.AanmaakForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TbLinks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudZeepPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NudTijd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TbRechts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar TbLinks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtAanmaken;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox ChLinks;
        private System.Windows.Forms.CheckBox ChRechts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown NudZeepPercentage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown NudTijd;
        private System.Windows.Forms.Label LbSelected;
        private System.Windows.Forms.TrackBar TbRechts;
        private System.Windows.Forms.Button BtBack;
    }
}