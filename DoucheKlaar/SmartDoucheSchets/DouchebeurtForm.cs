﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.IO.Ports;

namespace SmartDoucheSchets
{
    public partial class DouchebeurtForm : Form
    {
        int persoonId;
        private SerialPort LCDport;
        private SerialPort Motorport;

        Classes.MaakArduinoString LCD = new Classes.MaakArduinoString(9600, "COM3");
        Classes.MaakArduinoString MOTOR = new Classes.MaakArduinoString(9600, "COM7");
        Classes.Motors MotorA = new Classes.Motors("A", 20);
        Classes.Motors MotorB = new Classes.Motors("B", 20);

        SQL_Queries queries = new SQL_Queries();

        Timer DoucheTimer = new Timer();
        DoucheClass Behandeling;
        List<int> IdList = new List<int>();

        private string motoren;
        private int tijdverstreken;

        public DouchebeurtForm(int persoonId)
        {
            InitializeComponent();
            this.persoonId = persoonId;
            persoonId = Convert.ToInt16(persoonId);
            this.Cursor = new Cursor(GetType(), "Cursor1.cur");
        }
        private void DouchebeurtForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                DoucheTimer.Stop();
                Motorport.WriteLine(MOTOR.StopMotoren());

                Motorport.Close();
                LCDport.Close();
            }
            catch {}
            finally
            {
                Application.Exit();
            }
        }

        public void ListView()
        {
            LvBehandelingen.Items.Clear();
            LvBehandelingen.View = View.Details;
            try
            {
                DoucheDataTableAdapters.douchebeurtTableAdapter adapter = new DoucheDataTableAdapters.douchebeurtTableAdapter();
                DataTable dt = adapter.GetBeurten(persoonId);
                foreach (DataRow dr in dt.Rows)
                {
                    ListViewItem item = new ListViewItem(dr["Naam"].ToString());
                    item.SubItems.Add((dr["SnelheidLinks"] + "%").ToString());
                    item.SubItems.Add((dr["SnelheidRechts"] + "%").ToString());
                    item.SubItems.Add((dr["AantalZeep"] + "%").ToString());
                    item.SubItems.Add(dr["Tijd"].ToString());
                    LvBehandelingen.Items.Add(item);

                    IdList.Add(Convert.ToInt16(dr["Id"]));
                }
            }
            catch
            {
                MessageBox.Show("Er is een probleem met het ophalen van de gegevens", "ERROR");
            }
        }

        private void BtBack_Click(object sender, EventArgs e)
        {
            try
            {
                DoucheTimer.Stop();
                Motorport.WriteLine(MOTOR.StopMotoren());

                Motorport.Close();
                LCDport.Close();
            }
            catch
            {
                MessageBox.Show("ERROR");
            }
            finally
            {
                AccountForm form = new AccountForm();
                form.Show();
                this.Hide();
            }
        }
        
        private void Init()
        {
            try
            {
                LCDport = new SerialPort
                {
                    BaudRate = LCD.BaudRate,
                    PortName = LCD.PortName
                };
                LCDport.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Error with the connection of the LCD", "ERROR");
                LCDport.Close();
                BtStart.Enabled = false;
            }

            try
            {
                Motorport = new SerialPort
                {
                    BaudRate = MOTOR.BaudRate,
                    PortName = MOTOR.PortName
                };
                Motorport.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Error with the connection of the Motors", "ERROR");
                Motorport.Close();
                BtStart.Enabled = false;
                Application.Restart();
            }
        }

        private void DouchenbeurtForm_Load(object sender, EventArgs e)
        {
            //Connecties met Arduino maken
            Init();
            ListView();

            DoucheTimer.Interval = 1000;
            DoucheTimer.Tick += new EventHandler(DoucheTimer_Tick);
        }

        int pro = 0;

        private void BtStart_Click(object sender, EventArgs e)
        {
            int behandelingId = IdList[LvBehandelingen.FocusedItem.Index];
            string Name = queries.NaamOphalen(persoonId);

            BorstelClass[] borstels = queries.BorstelsOphalen(behandelingId);

            int Time = queries.DoucheTimer(behandelingId);
            Console.WriteLine(Time.ToString());//debug
            Behandeling = new DoucheClass(behandelingId, Name, borstels, Time);

            //Database info:
            string motorACommando = MotorA.MakeMotorCommando(borstels[0].Aan, borstels[0].Borstelsterkte);
            string motorBCommando = MotorB.MakeMotorCommando(borstels[1].Aan, borstels[1].Borstelsterkte);
            motoren = motorACommando + "&" + motorBCommando;

            //DEBUG
            Console.WriteLine(LCD.Stuur(Name));
            Console.WriteLine(MOTOR.Stuur(motoren) + " Procent: " + pro.ToString());

            //Berichten Sturen
            LCDport.WriteLine(LCD.Stuur(Name));
            Motorport.WriteLine(MOTOR.Stuur(motoren));

            tijdverstreken = 0;
            DoucheTimer.Start();
        }

        private void DoucheTimer_Tick(object sender, EventArgs e)
        {
            tijdverstreken++;

            if (tijdverstreken > Behandeling.Time)
            {
                //De motoren worden allen gestopt
                Motorport.WriteLine(MOTOR.StopMotoren());

                DoucheTimer.Stop();
                MessageBox.Show("Klaar!");
            }
        }

        private void BtVerwijder_Click(object sender, EventArgs e)
        {
            try
            {
                if (queries.VerwijderBeurt(IdList[LvBehandelingen.FocusedItem.Index]))
                {
                    IdList.Remove(LvBehandelingen.FocusedItem.Index);
                    ListView();
                }
                else
                {
                    MessageBox.Show("Mislukt om de behandeling te verwijderen");
                }
            }
            catch
            {
                MessageBox.Show("Selecteer een item uit de display");
            }
        }
    }
}