﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartDoucheSchets
{
    public partial class AccountForm : Form
    {

        int persoonId;
        List<int> PersoonsList = new List<int>();
        SQL_Queries sql = new SQL_Queries();

        public AccountForm()
        {
            InitializeComponent();
            this.Cursor = new Cursor(GetType(), "Cursor1.cur");
            CbSelecteerAccount.DataSource = sql.GetAccount();  // Combobox wordt gevuldt met de query als datasource
            PersoonsList = sql.GetIds();
        }

        private void BtStartDouchebeurt_Click(object sender, EventArgs e)
        {
            var Douchen = new DouchebeurtForm(persoonId);
            this.Hide();
            Douchen.Show(); // nieuwe form douchen wordt aangemaakt en het ID van de gebruiker wordt doorgestuurd

        }

        private void BtNieuwDouchebeurt_Click(object sender, EventArgs e)
        {
            AanmaakForm douche = new AanmaakForm(persoonId);
            douche.Show();
            this.Hide(); // nieuwe DoucheForm wordt aangemaakt en het ID van de gebruiker wordt doorgestuurd
        }

        private void BtMaakAccount_Click(object sender, EventArgs e)
        {
            AccountCreatieForm acc = new AccountCreatieForm();
            acc.Show();
            this.Hide(); // Het account registratie form wordt aangemaakt en geopend
        }

        private void AccountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit(); // Mocht het form op een niet gebruikelijke manier afgesloten worden wordt ook meteen de volledige applicatie gesloten
        }

        private void CbSelecteerAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            persoonId = CbSelecteerAccount.SelectedIndex + 1;
        }

        private void BtVerwijder_Click(object sender, EventArgs e)
        {
            try
            {
                DoucheDataTableAdapters.accountTableAdapter ada = new DoucheDataTableAdapters.accountTableAdapter();
                ada.DeleteAccount(PersoonsList[CbSelecteerAccount.SelectedIndex]);
                CbSelecteerAccount.DataSource = sql.GetAccount();
            }
            catch
            {
                MessageBox.Show("Dit werkt niet!");
            }
        }

        private void AccountForm_Load(object sender, EventArgs e)
        {

        }
    }
}
