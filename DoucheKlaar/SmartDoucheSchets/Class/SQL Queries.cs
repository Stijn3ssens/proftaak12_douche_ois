﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SmartDoucheSchets
{
    class SQL_Queries
    {
        readonly SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Douche.mdf;Integrated Security=True");

        public List<string> GetAccount()
        {
            try
            {
                string getNaam = "SELECT naam FROM account";
                SqlDataAdapter getNaamSQL = new SqlDataAdapter(getNaam, con);
                DataTable dt = new DataTable();
                getNaamSQL.Fill(dt);
                int x = 0;
                List<string> name = new List<string>();
                foreach (var naam in dt.Rows)
                {
                    DataRow row = dt.Rows[x];
                    x++;
                    name.Add(row["naam"].ToString());
                }
                return name;
            }
            catch
            {
                return null;
            }
        }

        public int DoucheTimer(int id)
        {
            try
            {
                int time;
                string timerQuery = $"SELECT Tijd FROM douchebeurt WHERE Id = {id}";
                SqlDataAdapter adapter = new SqlDataAdapter(timerQuery, con);
                DataTable table = new DataTable();
                adapter.Fill(table);
                DataRow idRow = table.Rows[0];
                time = Convert.ToInt16(idRow["Tijd"]);
                return time;
            }
            catch
            {
                return 0;
            }
        }

        public BorstelClass[] BorstelsOphalen(int id)
        {
            try
            {
                BorstelClass[] borstels = new BorstelClass[2];
                string borstelQuery = $"SELECT SnelheidLinks, SnelheidRechts, BorstelLinks, BorstelRechts FROM douchebeurt WHERE Id = {id}";
                SqlDataAdapter adapter = new SqlDataAdapter(borstelQuery, con);
                DataTable table = new DataTable();
                adapter.Fill(table);
                DataRow dr = table.Rows[0];
                borstels[0] = new BorstelClass(0, "A", Convert.ToInt16(dr["SnelheidLinks"]), Convert.ToBoolean(dr["BorstelLinks"]));
                borstels[1] = new BorstelClass(1, "B", Convert.ToInt16(dr["SnelheidRechts"]), Convert.ToBoolean(dr["BorstelRechts"]));

                return borstels;
            }
            catch
            {
                return null;
            }
        }

        public string NaamOphalen(int id)
        {
            try
            {
                string naamQuery = $"SELECT Naam FROM account WHERE Id = {id}";
                SqlDataAdapter adapter = new SqlDataAdapter(naamQuery, con);
                DataTable table = new DataTable();
                adapter.Fill(table);
                DataRow dr = table.Rows[0];
                string naam = dr["Naam"].ToString();
                return naam;
            }
            catch
            {
                return null;
            }
        }

        public bool VerwijderBeurt(int Id)
        {
            try
            {
                DoucheDataTableAdapters.douchebeurtTableAdapter ada = new DoucheDataTableAdapters.douchebeurtTableAdapter();
                ada.DeleteBeurt(Id);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool VerwijderPersoon(int Id)
        {
            try
            {
                DoucheDataTableAdapters.accountTableAdapter ada = new DoucheDataTableAdapters.accountTableAdapter();
                ada.DeleteAccount(Id);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public List<int> GetIds()
        {
            List<int> idList = new List<int>();
            string idQuery = $"SELECT Id FROM Account";
            SqlDataAdapter adapter = new SqlDataAdapter(idQuery, con);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            foreach(DataRow dr in dt.Rows)
            {
                idList.Add(Convert.ToInt16(dr["Id"]));
            }
            return idList;
        }
    }
}