﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets
{
    class DoucheClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BorstelClass[] Borstels { get; set; }
        public int Time { get; set; }

        public DoucheClass(int id, string name, BorstelClass[] borstels, int time)
        {
            Id = id;
            Name = name;
            Borstels = borstels;
            Time = time;
        }
    }
}
