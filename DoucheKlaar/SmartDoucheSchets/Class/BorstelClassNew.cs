﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets
{
    class BorstelClassNew
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Borstelsterkte { get; set; }

        public BorstelClassNew(int id, string name, int borstelsterkte)
        {
            Id = id;
            Name = name;
            Borstelsterkte = borstelsterkte;
        }
    }
}
