﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets.Classes
{
    class Motors
    {
        public string Motor { get; set; }
        public int MotorMinSpeed { get; set; }
        private int MotorPercentage;

        public Motors(string motor, int motorMinSpeed)
        {
            Motor = motor;
            MotorMinSpeed = motorMinSpeed;
            MotorPercentage = 100 / MotorMinSpeed;
        }

        public string MakeMotorCommando(bool motorAan, int motorSpeedProcent)
        {
            string motorCommando = Motor + Convert.ToInt16(motorAan);

            if (motorAan == true)
            {
                int speedMotor = Convert.ToInt16(motorSpeedProcent / MotorPercentage);
                speedMotor = MotorMinSpeed - speedMotor;
                motorCommando = motorCommando + "_" + speedMotor;
            }
            else
            {
                motorCommando = motorCommando + "_0";
            }

            return motorCommando;
        }
    }
}

//#A1_20&B1_20% Procent: 1
//#A1_20&B1_20% Procent: 2
//#A1_20&B1_20% Procent: 3

//#A1_19&B1_19% Procent: 4
//#A1_19&B1_19% Procent: 5
//#A1_19&B1_19% Procent: 6
//#A1_19&B1_19% Procent: 7
//#A1_19&B1_19% Procent: 8

//#A1_18&B1_18% Procent: 9
//#A1_18&B1_18% Procent: 10
//#A1_18&B1_18% Procent: 11
//#A1_18&B1_18% Procent: 12
//#A1_18&B1_18% Procent: 13

//#A1_17&B1_17% Procent: 14
//#A1_17&B1_17% Procent: 15
//#A1_17&B1_17% Procent: 16
//#A1_17&B1_17% Procent: 17
//#A1_17&B1_17% Procent: 18

//#A1_16&B1_16% Procent: 19
//#A1_16&B1_16% Procent: 20
//#A1_16&B1_16% Procent: 21
//#A1_16&B1_16% Procent: 22
//#A1_16&B1_16% Procent: 23

//#A1_15&B1_15% Procent: 24
//#A1_15&B1_15% Procent: 25
//#A1_15&B1_15% Procent: 26
//#A1_15&B1_15% Procent: 27
//#A1_15&B1_15% Procent: 28

//#A1_14&B1_14% Procent: 29
//#A1_14&B1_14% Procent: 30
//#A1_14&B1_14% Procent: 31
//#A1_14&B1_14% Procent: 32
//#A1_14&B1_14% Procent: 33

//#A1_13&B1_13% Procent: 34
//#A1_13&B1_13% Procent: 35
//#A1_13&B1_13% Procent: 36
//#A1_13&B1_13% Procent: 37
//#A1_13&B1_13% Procent: 38

//#A1_12&B1_12% Procent: 39
//#A1_12&B1_12% Procent: 40
//#A1_12&B1_12% Procent: 41
//#A1_12&B1_12% Procent: 42
//#A1_12&B1_12% Procent: 43

//#A1_11&B1_11% Procent: 44
//#A1_11&B1_11% Procent: 45
//#A1_11&B1_11% Procent: 46
//#A1_11&B1_11% Procent: 47
//#A1_11&B1_11% Procent: 48

//#A1_10&B1_10% Procent: 49
//#A1_10&B1_10% Procent: 50
//#A1_10&B1_10% Procent: 51
//#A1_10&B1_10% Procent: 52
//#A1_10&B1_10% Procent: 53

//#A1_9&B1_9% Procent: 54
//#A1_9&B1_9% Procent: 55
//#A1_9&B1_9% Procent: 56
//#A1_9&B1_9% Procent: 57
//#A1_9&B1_9% Procent: 58

//#A1_8&B1_8% Procent: 59
//#A1_8&B1_8% Procent: 60
//#A1_8&B1_8% Procent: 61
//#A1_8&B1_8% Procent: 62
//#A1_8&B1_8% Procent: 63

//#A1_7&B1_7% Procent: 64
//#A1_7&B1_7% Procent: 65
//#A1_7&B1_7% Procent: 66
//#A1_7&B1_7% Procent: 67
//#A1_7&B1_7% Procent: 68

//#A1_6&B1_6% Procent: 69
//#A1_6&B1_6% Procent: 70
//#A1_6&B1_6% Procent: 71
//#A1_6&B1_6% Procent: 72
//#A1_6&B1_6% Procent: 73

//#A1_5&B1_5% Procent: 74
//#A1_5&B1_5% Procent: 75
//#A1_5&B1_5% Procent: 76
//#A1_5&B1_5% Procent: 77
//#A1_5&B1_5% Procent: 78

//#A1_4&B1_4% Procent: 79
//#A1_4&B1_4% Procent: 80
//#A1_4&B1_4% Procent: 81
//#A1_4&B1_4% Procent: 82
//#A1_4&B1_4% Procent: 83

//#A1_3&B1_3% Procent: 84
//#A1_3&B1_3% Procent: 85
//#A1_3&B1_3% Procent: 86
//#A1_3&B1_3% Procent: 87
//#A1_3&B1_3% Procent: 88

//#A1_2&B1_2% Procent: 89
//#A1_2&B1_2% Procent: 90
//#A1_2&B1_2% Procent: 91
//#A1_2&B1_2% Procent: 92
//#A1_2&B1_2% Procent: 93

//#A1_1&B1_1% Procent: 94
//#A1_1&B1_1% Procent: 95
//#A1_1&B1_1% Procent: 96
//#A1_1&B1_1% Procent: 97
//#A1_1&B1_1% Procent: 98

//#A1_0&B1_0% Procent: 99
//#A1_0&B1_0% Procent: 100