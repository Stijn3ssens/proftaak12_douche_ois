﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SmartDoucheSchets
{
    class SQL_Queries_New
    {
        readonly SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Douche.mdf;Integrated Security=True");



        //Hier wordt de Naam van ieder account opgehaald
        public List<string> GetAccount()
        {
            try
            {
                string getNaam = "SELECT DBAccount.Naam FROM DBAccount ORDER BY DBAccount.Naam";
                SqlDataAdapter getNaamSQL = new SqlDataAdapter(getNaam, con);
                DataTable dt = new DataTable();
                getNaamSQL.Fill(dt);
                int x = 0;
                List<string> NaamList = new List<string>();
                foreach (var naam in dt.Rows)
                {
                    DataRow row = dt.Rows[x];
                    x++;
                    NaamList.Add(row["Naam"].ToString());
                }
                return NaamList;
            }
            catch
            {
                return null;
            }
        }



        //Hier wordt de Timer van de behandeling opgehaald
        public int DoucheTimer(int id)
        {
            try
            {
                string timerQuery = $"SELECT Tijd FROM DBBehandeling WHERE Id = {id}";
                SqlDataAdapter adapter = new SqlDataAdapter(timerQuery, con);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                DataRow idRow = dt.Rows[0];
                return Convert.ToInt16(idRow["Tijd"]); ;
            }
            catch
            {
                return 0;
            }
        }



        //Hier wordt een behandeling met diens borstels opgehaald
        public List<BorstelClass> BorstelsOphalen(int id)
        {
            try
            {
                List<BorstelClass> BorstelList = new List<BorstelClass>();

                string borstelQuery = "SELECT DBMotor.Id, DBMotor.Naam, DBMotor.AantalStanden, DBMotorBehandeling.Snelheid " +
                                        "FROM DBBehandeling " +
                                        "INNER JOIN DBMotorBehandeling ON DBBehandeling.Id = DBMotorBehandeling.BehandelingId " +
                                        "INNER JOIN DBMotor ON DBMotorBehandeling.MotorId = DBMotor.Id " +
                                       $"WHERE DBBehandeling.Id = {id} " +
                                        "ORDER BY DBMotor.Naam ASC;";
                SqlDataAdapter adapter = new SqlDataAdapter(borstelQuery, con);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    BorstelClass NewBorstel = new BorstelClass(Convert.ToInt16(dr["Id"]), (dr["Naam"]).ToString(), Convert.ToInt16(dr["Snelheid"]), true);
                    BorstelList.Add(NewBorstel);
                }

                return BorstelList;
            }
            catch
            {
                return null;
            }
        }



        //Hier wordt één naam opgehaald uit het database
        public string NaamOphalen(int id)
        {
            try
            {
                string naamQuery = $"SELECT DBAccount.Naam FROM DBAccount WHERE Id = {id}";
                SqlDataAdapter adapter = new SqlDataAdapter(naamQuery, con);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                DataRow dr = dt.Rows[0];
                return dr["Naam"].ToString();
            }
            catch
            {
                return null;
            }
        }



        //Hier wordt eerst de MotorBehandeling verwijderd
        //Vervolgens wordt de Behandeling zelf verwijderd
        public bool VerwijderBeurt(int Id)
        {
            try
            {
                DoucheDataTableAdapters.DBMotorBehandelingTableAdapter MotorBehandelingAdapter = new DoucheDataTableAdapters.DBMotorBehandelingTableAdapter();
                MotorBehandelingAdapter.DeleteMotorBehandelingenVanBehandeling(Id);

                DoucheDataTableAdapters.DBBehandelingTableAdapter BehandelingAdapter = new DoucheDataTableAdapters.DBBehandelingTableAdapter();
                BehandelingAdapter.DeleteBehandeling(Id);
            }
            catch
            {
                return false;
            }
            return true;
        }



        //Hier worden eerst de MotorBehandelingen verwijderd die horen bij een persoon
        //Vervolgens worden de Behandelingen van een Persoon verwijderd
        //Als laatste wordt de persoon verwijderd
        public bool VerwijderPersoon(int Id)
        {
            try
            {
                string naamQuery = $"SELECT Id FROM DBBehandeling WHERE AccountId = {Id}";
                SqlDataAdapter adapter = new SqlDataAdapter(naamQuery, con);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                List<int> arr = new List<int>();
                foreach (DataRow dr in dt.Rows)
                {
                    arr.Add(Convert.ToInt16(dr["Id"]));
                }

                foreach (int element in arr)
                {
                    DoucheDataTableAdapters.DBMotorBehandelingTableAdapter MotorBehandelingAdapter = new DoucheDataTableAdapters.DBMotorBehandelingTableAdapter();
                    MotorBehandelingAdapter.DeleteMotorBehandelingenVanBehandeling(element);
                }

                DoucheDataTableAdapters.DBBehandelingTableAdapter BehandelingAdapter = new DoucheDataTableAdapters.DBBehandelingTableAdapter();
                BehandelingAdapter.DeleteBehandelingenVanAccount(Id);

                DoucheDataTableAdapters.DBAccountTableAdapter AccountAdapter = new DoucheDataTableAdapters.DBAccountTableAdapter();
                AccountAdapter.DeleteAccount(Id);
            }
            catch
            {
                return false;
            }
            return true;


        }



        //Hier worden de Ids opgevraagd van alle Accounts
        public List<int> GetIds()
        {
            List<int> idList = new List<int>();
            string idQuery = $"SELECT DBAccount.Id FROM DBAccount";
            SqlDataAdapter adapter = new SqlDataAdapter(idQuery, con);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                idList.Add(Convert.ToInt16(dr["Id"]));
            }
            return idList;
        }
    }
}