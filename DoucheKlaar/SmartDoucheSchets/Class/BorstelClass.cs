﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDoucheSchets
{
    class BorstelClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Borstelsterkte { get; set; }
        public bool Aan { get; set; }

        public BorstelClass(int id, string name, int borstelsterkte, bool aan)
        {
            Id = id;
            Name = name;
            Borstelsterkte = borstelsterkte;
            Aan = aan;
        }
    }
}
