﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartDoucheSchets
{
    public partial class AanmaakForm : Form
    {
        int PersoonId;
        int[] values = new int[2];

        public AanmaakForm(int PersoonId)
        {
            InitializeComponent();
            this.Cursor = new Cursor(GetType(), "Cursor1.cur");
            this.PersoonId = PersoonId;
        }

        private void BtAanmaken_Click(object sender, EventArgs e)
        {
            if (ChLinks.Checked == false)
            {
                values[0] = 0;
            }
            if (ChRechts.Checked == false)
            {
                values[1] = 0;
            }

            try
            {
                DoucheDataTableAdapters.douchebeurtTableAdapter douche = new DoucheDataTableAdapters.douchebeurtTableAdapter();
                douche.QueryInsert("A", values[0], values[1], Convert.ToInt16(NudTijd.Value), Convert.ToInt16(ChLinks.Checked), Convert.ToInt16(ChRechts.Checked), Convert.ToDecimal(NudZeepPercentage.Value), PersoonId);
            }
            catch(Exception q)
            {
                MessageBox.Show($"error: {q}", "FATALE FOUT");
            }
            finally
            {
                DouchebeurtForm form = new DouchebeurtForm(PersoonId);
                form.Show();
                this.Hide();
            }
        }

        private void TrackbarValueChanged(object sender, EventArgs e)
        {
            TrackBar T = (TrackBar)sender;
            int tag = Convert.ToInt16(T.Tag);
            values[tag] = T.Value;
        }

        private void AanmaakForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void BtBack_Click(object sender, EventArgs e)
        {
            AccountForm form = new AccountForm();
            this.Hide();
            form.Show();
        }

        private void AanmaakForm_Load(object sender, EventArgs e)
        {
            values[0] = 1;
            values[1] = 1;
        }
    }
}