﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
namespace ArduinoTest
{
    public partial class Form1 : Form
    {
        private SerialPort myport;

        public Form1()
        {
            InitializeComponent();
            init();
        }
        private void init()
        {
            try
            {
                myport = new SerialPort();
                myport.BaudRate = 9600;
                myport.PortName = "COM5";
                myport.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }
        }

        string motor1 = "A0";
        string motor2 = "B0";
        //A1 betekent dat motor A aan gaat
        //B0 betekent dat motor B uit gaat

        private void BtMotor1aan_Click(object sender, EventArgs e)
        {
            motor1 = "A1";
            BtMotor1aan.Visible = false;
            BtMotor1uit.Visible = true;
        }

        private void BtMotor1uit_Click(object sender, EventArgs e)
        {
            motor1 = "A0";
            BtMotor1aan.Visible = true;
            BtMotor1uit.Visible = false;
        }

        private void BtMotor2aan_Click(object sender, EventArgs e)
        {
            motor2 = "B1";
            BtMotor2aan.Visible = false;
            BtMotor2uit.Visible = true;
        }

        private void BtMotor2uit_Click(object sender, EventArgs e)
        {
            motor2 = "B0";
            BtMotor2aan.Visible = true;
            BtMotor2uit.Visible = false;
        }


        //Het bericht wordt verstuurd door motor1 en motor2 door te sturen.
        private void BtSendMessage_Click(object sender, EventArgs e)
        {
            myport.WriteLine(motor1 + motor2);
            motor1 = "";
            motor2 = "";
        }
    }
}