﻿namespace ArduinoTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtMotor1aan = new System.Windows.Forms.Button();
            this.BtMotor1uit = new System.Windows.Forms.Button();
            this.BtMotor2aan = new System.Windows.Forms.Button();
            this.BtMotor2uit = new System.Windows.Forms.Button();
            this.BtSendMessage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtMotor1aan
            // 
            this.BtMotor1aan.BackColor = System.Drawing.Color.Lime;
            this.BtMotor1aan.Location = new System.Drawing.Point(11, 11);
            this.BtMotor1aan.Margin = new System.Windows.Forms.Padding(2);
            this.BtMotor1aan.Name = "BtMotor1aan";
            this.BtMotor1aan.Size = new System.Drawing.Size(254, 157);
            this.BtMotor1aan.TabIndex = 0;
            this.BtMotor1aan.Text = "ON";
            this.BtMotor1aan.UseVisualStyleBackColor = false;
            this.BtMotor1aan.Visible = false;
            this.BtMotor1aan.Click += new System.EventHandler(this.BtMotor1aan_Click);
            // 
            // BtMotor1uit
            // 
            this.BtMotor1uit.BackColor = System.Drawing.Color.Red;
            this.BtMotor1uit.Location = new System.Drawing.Point(335, 11);
            this.BtMotor1uit.Margin = new System.Windows.Forms.Padding(2);
            this.BtMotor1uit.Name = "BtMotor1uit";
            this.BtMotor1uit.Size = new System.Drawing.Size(254, 157);
            this.BtMotor1uit.TabIndex = 1;
            this.BtMotor1uit.Text = "OFF";
            this.BtMotor1uit.UseVisualStyleBackColor = false;
            this.BtMotor1uit.Click += new System.EventHandler(this.BtMotor1uit_Click);
            // 
            // BtMotor2aan
            // 
            this.BtMotor2aan.BackColor = System.Drawing.Color.Lime;
            this.BtMotor2aan.Location = new System.Drawing.Point(11, 198);
            this.BtMotor2aan.Margin = new System.Windows.Forms.Padding(2);
            this.BtMotor2aan.Name = "BtMotor2aan";
            this.BtMotor2aan.Size = new System.Drawing.Size(254, 157);
            this.BtMotor2aan.TabIndex = 2;
            this.BtMotor2aan.Text = "ON";
            this.BtMotor2aan.UseVisualStyleBackColor = false;
            this.BtMotor2aan.Visible = false;
            this.BtMotor2aan.Click += new System.EventHandler(this.BtMotor2aan_Click);
            // 
            // BtMotor2uit
            // 
            this.BtMotor2uit.BackColor = System.Drawing.Color.Red;
            this.BtMotor2uit.Location = new System.Drawing.Point(335, 198);
            this.BtMotor2uit.Margin = new System.Windows.Forms.Padding(2);
            this.BtMotor2uit.Name = "BtMotor2uit";
            this.BtMotor2uit.Size = new System.Drawing.Size(254, 157);
            this.BtMotor2uit.TabIndex = 3;
            this.BtMotor2uit.Text = "OFF";
            this.BtMotor2uit.UseVisualStyleBackColor = false;
            this.BtMotor2uit.Click += new System.EventHandler(this.BtMotor2uit_Click);
            // 
            // BtSendMessage
            // 
            this.BtSendMessage.BackColor = System.Drawing.Color.Orange;
            this.BtSendMessage.Location = new System.Drawing.Point(11, 371);
            this.BtSendMessage.Margin = new System.Windows.Forms.Padding(2);
            this.BtSendMessage.Name = "BtSendMessage";
            this.BtSendMessage.Size = new System.Drawing.Size(578, 76);
            this.BtSendMessage.TabIndex = 4;
            this.BtSendMessage.Text = "Versturen";
            this.BtSendMessage.UseVisualStyleBackColor = false;
            this.BtSendMessage.Click += new System.EventHandler(this.BtSendMessage_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(600, 458);
            this.Controls.Add(this.BtSendMessage);
            this.Controls.Add(this.BtMotor2uit);
            this.Controls.Add(this.BtMotor2aan);
            this.Controls.Add(this.BtMotor1uit);
            this.Controls.Add(this.BtMotor1aan);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtMotor1aan;
        private System.Windows.Forms.Button BtMotor1uit;
        private System.Windows.Forms.Button BtMotor2aan;
        private System.Windows.Forms.Button BtMotor2uit;
        private System.Windows.Forms.Button BtSendMessage;
    }
}

